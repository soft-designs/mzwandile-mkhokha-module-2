main() {
//Dart 2 Create an array to store all the winning apps of the MTN Business App of the Year Awards since 2012
 	    var names = [ //Array that stores all the winning Apps since 2012
  "FNB App         Year : 2012",
  "Snap Scan       Year : 2013",
  "Pineapple       Year : 2014",
  "Wumdrop         Year : 2015",
  "Domestly        Year : 2016",
  "Shyft           Year : 2017",
  "SnupIt          Year : 2018",
  "Naked Insurance Year : 2019",
  "EasyEquities    Year : 2020",
  "Ambani Africa   Year : 2021"
];
  //Dart 2, Question A) Sort and print the apps by name; 
names.sort();// Sort Array items Alphabetically

//loop that prints the array items 
  print("The winners MTN App of the year from sorted in alphabetic order ");
  for( int i=0;;i<names.length;i++)
  {
    print(names[i]);
  }

 //Dart 2, Question B) Print the winning app of 2017 and the winning app of 2018
 String winner= "MTN";   //Dummy strinng to store the winning App of the year specified
  int totalApps=0;       //integer to store the total number of the items on the array
  for(int i=0;i<names.length;i++)
  {
    totalApps++;
    if(names[i].contains("2017")){
      String winner= names[i].toString();
      print("");
      print("The winner of MTN App of the year 2017: " +winner.substring(0,15));
    }
    if(names[i].contains("2018")){
      String winner= names[i].toString();
      print("");
      print("The winner of MTN App of the year 2018: " +winner.substring(0,15));
    }
  }
  
  
  //Dart 2, Qustion C)the Print total number of apps from the array.
  print("");
  print("The total number of Apps in Array is "+ totalApps.toString());
}
